#!/bin/bash
cd node-hello-stratizens
#Remove Running Containers
docker rm -f $(docker ps -a -q)
#Remove Images
docker image rm -f $(docker image ls -a -q)
#Build Docker Image on EC2
docker image build -t romarcablao/node-hello-stratizens .
#Run and Expose to Port 8080
docker run -d -p 8080:8080 --name appv1 romarcablao/node-hello-stratizens
#List Running Containers
docker ps