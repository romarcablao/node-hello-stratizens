# gitlab-ec2-pipeline

Simple gitlab ci/cd - Node App

## Dependencies

1. [NodeJS](https://nodejs.org/en/download/)

2. [Docker](https://docs.docker.com/install/)

## Instructions

1. Clone the repository
   ```bash
   git clone https://gitlab.com/rcablao/node-hello-stratizens.git
   cd node-hello-stratizens
   ```
2. Simply push to this repository and check your update here: http://pipeline.thecloudspark.com/

---

[Romar Cablao](https://www.linkedin.com/in/romarcablao) | <romarcablao@gmail.com>
